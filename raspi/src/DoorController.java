import msg.*;
import java.io.IOException;
import java.util.*;
import java.io.*;

import common.BasicController;
import devices.Button;
import devices.Light;

public class DoorController extends BasicController {
	
	private Map<String, String> login; 
	private Light insideLed;
	private Light failedLed;
	private SerialCommChannel channel; 
	private File file;
	private Database data;

	public DoorController(Light insideLed, Light failedLed, String port) throws Exception {
		
		this.insideLed = insideLed;
		this.failedLed = failedLed;
		this.login = new HashMap<>();
		this.populate();
		this.channel = new SerialCommChannel(port,9600);
		this.file = Objects.requireNonNull(new File("log.txt"));
		this.data = new Database("consegna", "root", "");
	}

	public void run(){
		try {
			while(true) {
				if(channel.isMsgAvailable()) {
					String str = channel.receiveMsg();
					System.out.println("Ricevuto: " + str);
					String[] usr = str.split("!");
					/*
					String query = "SELECT * FROM Utenti WHERE utente = '" + usr[0] + "' AND password = '" + usr[1] +"'";
					if(data.eseguiQuery(query).size() == 1) {
						channel.sendMsg("ok");
						try (PrintWriter p = new PrintWriter(new BufferedWriter(new FileWriter("log.txt", true)))) {
							p.println("Utente: " + usr[0] + " accede");
							p.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					} else {
						channel.sendMsg("non");
						try (PrintWriter p = new PrintWriter(new BufferedWriter(new FileWriter("log.txt", true)))) {
							p.println("Accesso negato");
							p.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					*/
					if(this.login.containsKey(usr[0])) {
						if(this.login.get(usr[0]).equals(usr[1])) {
							channel.sendMsg("ok");
							insideLed.switchOn();
							try (PrintWriter p = new PrintWriter(new BufferedWriter(new FileWriter("log.txt", true)))) {
								p.println("Utente: " + usr[0] + " accede");
								p.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					} else {
						channel.sendMsg("non");
						failedLed.switchOn();
						waitFor(100);
						failedLed.switchOff();
						try (PrintWriter p = new PrintWriter(new BufferedWriter(new FileWriter("log.txt", true)))) {
							p.println("Accesso negato");
							p.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

				}
				waitFor(50);
			}
		} catch (InterruptedException ex){
			ex.printStackTrace();
		}
	}

	private void populate() {
		this.login.put("nicola", "123");
		this.login.put("gianni", "747");
		this.login.put("ghini", "666");
	}

}