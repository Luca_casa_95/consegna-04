import msg.*;
import java.io.*;

import devices.*;

public class TestDoor {

	public static void main(String[] args) throws Exception {	
		
		System.out.println("Waiting Arduino for rebooting...");		
		Thread.sleep(4000);
		System.out.println("Ready.");		

		Light insideLed = new devices.p4j_impl.Led(7);
		Light failedLed = new devices.p4j_impl.Led(2);
		new DoorController(insideLed,failedLed, args[0]).start();
	}

}