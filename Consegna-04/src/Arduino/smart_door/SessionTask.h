#ifndef __SESSIONTASK__
#define __SESSIONTASK__

#include "Task.h"
#include "ServoMot.h"
#include "LedExt.h"
#include "Pir.h"

class SessionTask: public Task {
  
public:

  SessionTask(int ledRossoPin, int servoPin); 
  void init(int period);  
  void tick();

private:

 ServoMot* myservo;
 LedExt* ledrosso;
};

#endif

