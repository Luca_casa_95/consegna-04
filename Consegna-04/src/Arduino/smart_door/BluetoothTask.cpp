#include "BluetoothTask.h"
#include "utility.h"
#include "Arduino.h"
#include "MsgService.h"
#include "SerialService.h"

extern volatile StateG GState;
extern volatile float gradi;
extern volatile int luminosita;

Msg* message; //messaggio  BT
int camp=0; //campionamento per la tolleranza
int t = millis(); //tempo attuale
int t2 = t + 3000; //tempo attuale + 3 secondi
boolean visto = false; //booleana per vedere se c'è qualcuno
boolean presenza = false; //booleana per il pir

BluetoothTask::BluetoothTask(int baud, int trigPin, int echoPin, int rx, int tx, int pirPin){
  //inizializzo seriale, sensore a ultrasuoni e bluetooth
   this->baud = baud; 
   this->sensor = new UltrasonicSensor(trigPin, echoPin);
   this->bluetooth = new MsgService(rx,tx);
   this->pir = new Pir(pirPin);
   bluetooth->init();
   Serial.begin(9600);
   luminosita = 0;
   SerialService.init();
}

void BluetoothTask::init(int period){
  Task::init(period);
}

void BluetoothTask::tick(){ 
  if(GState == A){
   /*all'inizio GState è A 
  *controllo per 3 secondi la distanza
  */
      for(int i = 0; i < CAMP; i++){
        //ogni volta che vedo qualcosa sotto a MIN_DIST incremento un contatore
        camp += sensor->getDistance() <= DIST_MIN ? 1 : 0;
      }    
      //Abbiamo fatto un controllo di tolleranza se secondo noi il prossimità sta vedendo veramente qualcosa   
      if(camp >= TOLL){
        //aggiorno t e t2 per fare partire da questo momento i 3 secondi
        t = millis();
        t2 = t + 3000; 
        while(t < t2){
          //per 3 secondi guardo la distanza e metto a ture la booleana
            if(sensor->getDistance() <= DIST_MIN){
              visto = true;
            }
            //con una serie di 3 campionamenti di fila controllo che la persona non si sia allontanata
            //oppure che il campionamento non si sballi. Nel caso setto a false la booleanaa
            if(sensor->getDistance() > DIST_MIN){
               if(sensor->getDistance() > DIST_MIN){
                  if(sensor->getDistance() > DIST_MIN){
                    visto = false;
                  }
               }
            }
            //aggiorno il tempo 
            t = millis();
        }
        if(visto){
          //1 per fare aprire la form di login su android
          this->bluetooth->sendMsg(Msg("1"));
        }else{
          //2 per far sparire la form di login su android
          this->bluetooth->sendMsg(Msg("2"));
        }
        //azzero
        camp = 0;
     }
      
      
     //controllo che sia ricevibile qualcosa
     if (bluetooth->isMsgAvailable()) {
        //ricevo il messaggio dentro "msg"
        message = bluetooth->receiveMsg();
        String msg = message->getContent();
        //finto controllo che mi è stata inviata la giusta password da raspberry
            if (msg.length() > 4){           
              t = millis();
              t2 = t + 3000; 
              //3 SECONDI IN CUI CONTROLLO SE IL PIR NON RILEVA LA PRESENZA
              while(t < t2){
                //per 3 secondi guardo il sensore PIR
                if(digitalRead(4) == HIGH){
                  if(digitalRead(4) == HIGH){
                    if(digitalRead(4) == HIGH){
                      presenza = true;
                    }
                  }
                }
                //aggiorno il tempo 
                t = millis();
              }
              if(presenza == true){
                //il pir ha rilevato la presenza
                SerialService.sendMsg(msg);
                GState = F;
                /*Serial.println("PRESENZA RILEVATA");
                Serial.println("APERTURA PORTA");
                this->bluetooth->sendMsg(Msg("accedi"));
                //cambio stato per far partire un altro task
                GState = B;
                presenza = false;*/
              }else if(presenza == false){
                GState = A;
              }
     }
    }
  } 
  if(GState == B){
   
    //mando i gradi in bluetooth a mobile
    this->bluetooth->sendMsg(Msg("t :" + String(gradi)));
    delay(50);
    //ricevo messaggio da bluetooth e in base a cos'è aumento e diminuisco la luminosità del led
     if (bluetooth->isMsgAvailable()) {
        message = bluetooth->receiveMsg();
        String msg = message->getContent();
        
        //Serial.println(msg);
        //mobile ha premuto il "+"
        if(msg == "si" && luminosita <= 225){
          luminosita = luminosita + 30;
        //mobile ha premuto il "-"
        }else if(msg == "no" && luminosita >= 30){
           luminosita = luminosita -30;
        //mobile ha premuto il "esci"
        }else if(msg == "esci"){
           GState = A;
          
        }
     }
  }
if(GState == F) {
  if(SerialService.isMsgAvailable()){
    String messaggino = SerialService.receiveMsg()->getContent();
    if(messaggino == "ok"){
      GState = B;
      this->bluetooth->sendMsg(Msg("accedi"));
    }else{
      GStato = A;
    }
  }
}
}



