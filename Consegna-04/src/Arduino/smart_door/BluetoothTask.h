#ifndef __BLUETOOTHTASK__
#define __BLUETOOTHTASK__

#include "Task.h"
#include "UltrasonicSensor.h"
#include "MsgService.h"
#include "Pir.h"

class BluetoothTask: public Task {
  int baud=9600;

public:

  BluetoothTask(int baud,int trigPin, int echoPin, int rx, int tx, int pirPin); 
  void init(int period);  
  void tick();

private:
  UltrasonicSensor* sensor;
  MsgService* bluetooth; 
  Pir* pir;
};

#endif

