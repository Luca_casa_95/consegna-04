#include "Pir.h"
#include "Arduino.h"

Pir::Pir(int pinPir){
  this->pin = pinPir;
}

bool Pir::detected(){
  return digitalRead(pin) == HIGH;
}

