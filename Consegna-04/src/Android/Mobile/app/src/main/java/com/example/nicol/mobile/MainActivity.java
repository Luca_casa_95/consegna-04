package com.example.nicol.mobile;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.UUID;


public class MainActivity extends AppCompatActivity {
    private int defenseDialogCount = 0;
    private int mailDialogCount = 0;
    final int ACCES_FINE_LOCATION_REQUEST = 1234;

    private static MainActivityHandler uiHandler;
    private String defenseValue;
    private AlertDialog gpsRequest;
    private AlertDialog mailDialog;
    private AlertDialog defense;
    private boolean notifyEnabled;
    private String mailAddress = null;
    private TextView serialMessage;
    private BluetoothAdapter btAdapter;
    private BluetoothDevice targetDevice;
    private LocationManager locationManager;
    private Location location;
    private AlertDialog ciao;

    private LocationListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_mobile);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        uiHandler = new MainActivityHandler(this);
        initUI();

    }

    private void initUI() {
    }

    protected void onStart() {
        super.onStart();
        this.findViewById(R.id.utente).setVisibility(View.INVISIBLE);
        this.findViewById(R.id.password).setVisibility(View.INVISIBLE);
        this.findViewById(R.id.filo).setVisibility(View.INVISIBLE);
        this.findViewById(R.id.temperatura).setVisibility(View.INVISIBLE);
        this.findViewById(R.id.piu).setVisibility(View.INVISIBLE);
        this.findViewById(R.id.meno).setVisibility(View.INVISIBLE);
        this.findViewById(R.id.esci).setVisibility(View.INVISIBLE);
        this.findViewById(R.id.filo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("ciao","casa");
                try {
                    String msg=((EditText)findViewById(R.id.utente)).getText().toString() + "!" +((EditText)findViewById(R.id.password)).getText().toString();
                    BluetoothConnectionManager.getInstance().sendMsg(msg);
                } catch (MsgTooBigException e) {
                    e.printStackTrace();
                }
            }
        });
        this.findViewById(R.id.esci).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    String msg = ("esci");
                    BluetoothConnectionManager.getInstance().sendMsg(msg);
                    findViewById(R.id.utente).setVisibility(View.INVISIBLE);
                    findViewById(R.id.password).setVisibility(View.INVISIBLE);
                    findViewById(R.id.filo).setVisibility(View.INVISIBLE);
                    findViewById(R.id.temperatura).setVisibility(View.INVISIBLE);
                    findViewById(R.id.piu).setVisibility(View.INVISIBLE);
                    findViewById(R.id.meno).setVisibility(View.INVISIBLE);
                    findViewById(R.id.esci).setVisibility(View.INVISIBLE);

                } catch (MsgTooBigException e){
                    e.printStackTrace();
                }
            }
        });

        this.findViewById(R.id.piu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String msg = "si";
                    BluetoothConnectionManager.getInstance().sendMsg(msg);
                } catch (MsgTooBigException e) {
                    e.printStackTrace();
                }
            }
        });

        this.findViewById(R.id.meno).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String msg = "no";
                    BluetoothConnectionManager.getInstance().sendMsg(msg);
                } catch (MsgTooBigException e) {
                    e.printStackTrace();
                }
            }
        });

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter != null) {
            if (btAdapter.isEnabled()) {
                targetDevice = BluetoothUtils.findPairedDevice(C.TARGET_BT_DEVICE_NAME, btAdapter);
                if (targetDevice != null) {
                    String t = "Target BT Device: Found " + targetDevice.getName();
                    connectToTargetbtDevice();
                }
            } else {
                startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), C.ENABLE_BT_REQUEST);
            }
        } else {
            showBluetoothUnavailableAlert();

        }
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        BluetoothConnectionManager.getInstance().cancel();

        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
    }

    private void connectToTargetbtDevice() {
        UUID uuid = UUID.fromString(C.TARGET_BT_DEVICE_UUID);

        BluetoothConnectionTask task = new BluetoothConnectionTask(this, targetDevice, uuid);
        task.execute();
    }

    private void showBluetoothUnavailableAlert() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Warning")
                .setMessage("Bluetooth unavailable on this device")
                .setCancelable(false)
                .setNeutralButton("Close APP", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        MainActivity.this.finish();
                    }
                })
                .create();
        dialog.show();
    }

    public static MainActivityHandler getHandler() {
        return uiHandler;
    }

    /**
     * The Handler Associated to the MainActivity Class
     */
    public class MainActivityHandler extends Handler {
        private final WeakReference<MainActivity> context;

        MainActivityHandler(MainActivity context) {
            this.context = new WeakReference<>(context);
        }

        /**
         * Metodo per la gestione dei casi
         * @param msg messagio dello stato
         */
        public void handleMessage(Message msg) {

            Object obj = msg.obj;
            if (obj instanceof String) {
                final String message = obj.toString();
                Log.d("ricevuto", "___"+message);
                switch (message) {
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.d("view", "on");
                                findViewById(R.id.utente).setVisibility(View.VISIBLE);
                                findViewById(R.id.password).setVisibility(View.VISIBLE);
                                findViewById(R.id.filo).setVisibility(View.VISIBLE);
                                findViewById(R.id.temperatura).setVisibility(View.INVISIBLE);
                                findViewById(R.id.piu).setVisibility(View.INVISIBLE);
                                findViewById(R.id.meno).setVisibility(View.INVISIBLE);
                                findViewById(R.id.esci).setVisibility(View.INVISIBLE);
                            }
                        });
                        break;
                    case "2":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.d("view", "on");
                                findViewById(R.id.utente).setVisibility(View.INVISIBLE);
                                findViewById(R.id.password).setVisibility(View.INVISIBLE);
                                findViewById(R.id.filo).setVisibility(View.INVISIBLE);
                                findViewById(R.id.temperatura).setVisibility(View.INVISIBLE);
                                findViewById(R.id.piu).setVisibility(View.INVISIBLE);
                                findViewById(R.id.meno).setVisibility(View.INVISIBLE);
                                findViewById(R.id.esci).setVisibility(View.INVISIBLE);
                            }
                        });
                        break;
                    case "accedi":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.d("mobile", "on");
                                findViewById(R.id.utente).setVisibility(View.INVISIBLE);
                                findViewById(R.id.password).setVisibility(View.INVISIBLE);
                                findViewById(R.id.filo).setVisibility(View.INVISIBLE);
                                findViewById(R.id.temperatura).setVisibility(View.VISIBLE);
                                findViewById(R.id.piu).setVisibility(View.VISIBLE);
                                findViewById(R.id.meno).setVisibility(View.VISIBLE);
                                findViewById(R.id.esci).setVisibility(View.VISIBLE);
                            }
                        });

                        break;
                    case "OUT":
                        context.get().serialMessage.setText("");
                        break;
                    default:
                        if (message.contains("t")) {
                            final String gradi = message;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ((TextView) findViewById(R.id.temperatura)).setText(gradi);
                                }
                            });
                        }
                        break;
                }
            }
        }
    }
}