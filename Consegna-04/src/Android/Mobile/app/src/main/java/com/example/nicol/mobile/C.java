package com.example.nicol.mobile;

public class C {
    public static final String LOG_TAG = "PSE-Example-01";

    static final String TARGET_BT_DEVICE_NAME = "ArduinoLuca";
    static final String TARGET_BT_DEVICE_UUID = "00001001-0000-1000-8000-00805F9B34FB";
    static final int ENABLE_BT_REQUEST = 1;

    static final String C_ACCESO_MESSAGE="A";
    static final String C_SPENTO_MESSAGE="C";
    static final String C_PARK_MESSAGE="B";

}