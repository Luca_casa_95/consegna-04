package com.example.nicol.mobile;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;


public class BluetoothConnectionTask extends AsyncTask<Void, Void, Boolean> {

    private BluetoothSocket btSocket = null ;
    private MainActivity context = null;

    public BluetoothConnectionTask(MainActivity context, BluetoothDevice server, UUID uuid){

        this.context = context;
        try {
            btSocket =(BluetoothSocket) server.getClass().getMethod("createRfcommSocket", new Class[] {int.class}).invoke(server,1);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

/*
        try {
            btSocket = server.createRfcommSocketToServiceRecord(uuid);
        } catch ( IOException e){
            e.printStackTrace();
        }*/
    }

    @Override
    protected Boolean doInBackground (Void ... params ){
        try{
            btSocket.connect();
            Log.d("caldero", "ciao");
        } catch(IOException connectException){
            try{
                btSocket.close();
            } catch(IOException closeException){
                closeException.printStackTrace();
            }

            connectException.printStackTrace();
            return false;
        }

        BluetoothConnectionManager cm = BluetoothConnectionManager.getInstance ();
        cm.setChannel(btSocket);
        cm.start();

        return true;
    }

   // @Override
   // protected void onPostExecute(Boolean connected) {
     //   TextView flagLabel = (TextView) context.findViewById(R.id.btConnectedFlagLabel);

      //  if(connected) {
       //     flagLabel.setText("Target BT Status: Connected");
       // } else {
       //     flagLabel.setText("Target BT Status: Not Connected");
       // }
    //}
}