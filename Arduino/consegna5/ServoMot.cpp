#include "ServoMot.h"
#include "Arduino.h"

ServoMot::ServoMot(int pin) {
  this->pin = pin;
  pinMode(pin, OUTPUT);
}
//il valore ricevuto come parametro è una percentuale d'azione:[0-100]
void ServoMot::act(int perc){
  //rimappo il valore da [0-100] a [0-2500]
  int angle=map(perc,0,100,0,2500);
  //faccio muovere il servo motore
  digitalWrite(pin, HIGH);
  delayMicroseconds(angle);
  digitalWrite(pin,LOW);
  delay(20-(angle/1000));
  
}
