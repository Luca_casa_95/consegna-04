#include "utility.h"
#include "Scheduler.h"
#include "BluetoothTask.h"
#include "SessionTask.h"

Scheduler sched;
volatile StateG GState; //variabile per lo stato
volatile float gradi; //variabile globale per la temperatura
volatile int luminosita; //variabile globale per la luminosità del led rosso

void setup()
{
   sched.init(100); 

  //task con bluetooth e sensore di prossimità
   Task* t0 = new BluetoothTask(9600, 9, 10, 3, 2, 4);
   t0->init(100);
   sched.addTask(t0);

  //task con servo , pir e led rosso
   Task* t1 = new SessionTask(5, 6);
   t1->init(100);
   sched.addTask(t1);

//all'inizio lo stato è A
    GState = A;
    pinMode(7, OUTPUT);
    pinMode(8, INPUT);
    pinMode(4, INPUT);

    luminosita = 0;

}
 
void loop()
{
  //pin che mi dice che arduino è acceso
  digitalWrite(7,HIGH);
  sched.schedule();
}
