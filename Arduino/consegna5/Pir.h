#ifndef __PIR__
#define __PIR__

class Pir{
  
  public:
  Pir(int pinPir);
  bool detected();

  private:
  int pin;
};

#endif
