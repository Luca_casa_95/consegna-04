#include "SessionTask.h"
#include "utility.h"
#include "Arduino.h"

extern volatile StateG GState; 
extern volatile float gradi;
extern volatile int luminosita;

int fl=0; //flag per mandare una volta solo il servo a 0 gradi
int fl2=0; //flag per mandare una volta solo il servo a 100 gradi
int val_Adc = 0; //variabile temporanea per il calcolo della temperatura
float temp = 0; //variabile temporanea per il calcolo della temperatura
float millivolts = 0; //variabile temporanea per il calcolo della temperatura
float celsius = 0; //variabile temporanea per il calcolo della temperatura

SessionTask::SessionTask(int ledRossoPin, int servoPin){
  this->ledrosso = new LedExt(ledRossoPin,0);
  this->myservo = new ServoMot(servoPin);
  Serial.begin(9600);
  luminosita = 0;
}

void SessionTask::init(int period){
  Task::init(period);
}

void SessionTask::tick(){
  if(GState == A && fl == 0){
      myservo->act(0);
      fl = 1;
  }
  
  if(GState == B){
    if(fl2 == 0){
      myservo->act(100);
      fl2 = 1;
    }
    
    //leggo dalla porta A0 e mi tiro fuori i gradi
    val_Adc = analogRead(0);
    millivolts = (val_Adc/1023.0)*5000;
    celsius = millivolts/10; 
    gradi = celsius;
    delay(50);

    //setto l'intensità
    ledrosso->switchOn();   
    ledrosso->setIntensity(luminosita);

    if(digitalRead(8) == HIGH){
      myservo->act(0);
      GState = A;
      fl2 = 0;
    }
  }
}

